AWSTemplateFormatVersion: '2010-09-09'
Transform: AWS::Serverless-2016-10-31
Description: 'url-shorter url-shorter #magic___^_^___line'
Globals:
  Function:
    Timeout: 3
    MemorySize: 128
    Runtime: python3.10
    Tracing: Active
    LoggingConfig:
      LogFormat: JSON
  Api:
    TracingEnabled: true


Parameters:
  UserPoolAdminGroupName:
    Description: User pool group name for API administrators 
    Type: String
    Default: apiAdmins


Resources:

  # Url Database
  UrlTable:
    Type: AWS::DynamoDB::Table
    Properties:
      TableName:
        Fn::Sub: ${AWS::StackName}-url
      AttributeDefinitions:
      - AttributeName: pk
        AttributeType: S
      - AttributeName: sk
        AttributeType: S
      KeySchema:
      - AttributeName: pk
        KeyType: HASH
      - AttributeName: sk
        KeyType: RANGE
      BillingMode: PAY_PER_REQUEST
    Metadata:
      SamResourceId: UrlTable
  # Post Url Function for create short url
  PostUrlFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: url_shorter.url_shortener
      Description: Handler for Url Post
      Environment:
        Variables:
          URL_TABLE:
            Ref: UrlTable
      Policies:
      - DynamoDBCrudPolicy:
          TableName:
            Ref: UrlTable
      Tags:
        Stack:
          Fn::Sub: ${AWS::StackName}
      Events:
        PostUrl:
          Type: Api
          Properties:
            Path: /createSecret
            Method: post
            RestApiId:
              Ref: RestAPI
            Auth:
              Authorizer: MyLambdaFunctionAuthorizer
    Metadata:
      SamResourceId: PostUrlFunction
  # Get By User Id Function for get all url by userid
  GetByIdFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: get_by_id.lambda_handler
      Description: Handler for get by id
      Environment:
        Variables:
          URL_TABLE:
            Ref: UrlTable
      Policies:
      - DynamoDBCrudPolicy:
          TableName:
            Ref: UrlTable
      Tags:
        Stack:
          Fn::Sub: ${AWS::StackName}
      Events:
        PostUrl:
          Type: Api
          Properties:
            Path: /getbyuserid/{id}
            Method: get
            RestApiId:
              Ref: RestAPI
            Auth:
              Authorizer: MyLambdaFunctionAuthorizer
            
            
    Metadata:
      SamResourceId: GetByIdFunction
  # get by short url function for get url by short url
  GetByShortUrlFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: get_by_short_url.lambda_handler
      Description: Handler for get by short url
      Environment:
        Variables:
          URL_TABLE:
            Ref: UrlTable
      Policies:
      - DynamoDBCrudPolicy:
          TableName:
            Ref: UrlTable
      Tags:
        Stack:
          Fn::Sub: ${AWS::StackName}
      Events:
        PostUrl:
          Type: Api
          Properties:
            Path: /getbyshorturl/{userid}/{shorturl}
            Method: get
            RestApiId:
              Ref: RestAPI
            Auth:
              Authorizer: MyLambdaFunctionAuthorizer
    Metadata:
      SamResourceId: GetByShortUrlFunction

  MyLambdaFunctionAuthorizer:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: authorizer.lambda_handler
      Description: Handler for authorizer
      Environment:
        Variables:
          USER_POOL_ID: !Ref UserPool
          APPLICATION_CLIENT_ID: !Ref UserPoolClient
          ADMIN_GROUP_NAME: !Ref UserPoolAdminGroupName
          REGION: !Ref AWS::Region

  
  # create short url function for add url to existing user
  CreateShorturlByUseridFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: create_shorturl_by_userid.lambda_handler
      Description: Handler for add url to existing user
      Environment:
        Variables:
          URL_TABLE:
            Ref: UrlTable
      Policies:
      - DynamoDBCrudPolicy:
          TableName:
            Ref: UrlTable
      Tags:
        Stack:
          Fn::Sub: ${AWS::StackName}
      Events:
        PostUrl:
          Type: Api
          Properties:
            Path: /addurl/{userid}
            Method: post
            RestApiId:
              Ref: RestAPI
            Auth:
              Authorizer: MyLambdaFunctionAuthorizer
    Metadata:
      SamResourceId: CreateShorturlByUseridFunction

  # delete short url function for delete url of a user
  DeleteShortUrlFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: delete_shorturl_by_shorturl.lambda_handler
      Description: Handler for delete short url
      Environment:
        Variables:
          URL_TABLE:
            Ref: UrlTable
      Policies:
      - DynamoDBCrudPolicy:
          TableName:
            Ref: UrlTable
      Tags:
        Stack:
          Fn::Sub: ${AWS::StackName}
      Events:
        PostUrl:
          Type: Api
          Properties:
            Path: /delete/shorturl/{shorturl}
            Method: delete
            RestApiId:
              Ref: RestAPI
            Auth:
              Authorizer: MyLambdaFunctionAuthorizer
    Metadata:
      SamResourceId: DeleteShortUrlFunction
  # update short url function for update url of a user by short url
  UpdateShortUrlFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: update_shorturl_by_shorturl.lambda_handler
      Description: Handler for update short url
      Environment:
        Variables:
          URL_TABLE:
            Ref: UrlTable
      Policies:
      - DynamoDBCrudPolicy:
          TableName:
            Ref: UrlTable
      Tags:
        Stack:
          Fn::Sub: ${AWS::StackName}
      Events:
        PostUrl:
          Type: Api
          Properties:
            Path: /update/shorturl/{shorturl}
            Method: PUT
            RestApiId:
              Ref: RestAPI
            Auth:
              Authorizer: MyLambdaFunctionAuthorizer
    Metadata:
      SamResourceId: UpdateShortUrlFunction
  # url redirect function for redirect to original url
  UrlRedirectFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: url_redirect.lambda_handler
      Description: Handler for url redirect
      Environment:
        Variables:
          URL_TABLE:
            Ref: UrlTable
      Policies:
      - DynamoDBCrudPolicy:
          TableName:
            Ref: UrlTable
      Tags:
        Stack:
          Fn::Sub: ${AWS::StackName}
      Events:
        PostUrl:
          Type: Api
          Properties:
            Path: /url/redirect/{shorturl}
            Method: GET
            RestApiId:
              Ref: RestAPI
    Metadata:
      SamResourceId: UrlRedirectFunction
  # rest api for all functions
  RestAPI:
    Type: AWS::Serverless::Api
    Properties:
      StageName: Prod
      TracingEnabled: true
      Auth:
        DefaultAuthorizer: MyLambdaFunctionAuthorizer
        Authorizers:
          MyLambdaFunctionAuthorizer:
            FunctionArn:
              !GetAtt MyLambdaFunctionAuthorizer.Arn
            Identity:
              Header: Authorization
            ResultTtlInSeconds: 300
            
      Tags:
        Name:
          Fn::Sub: ${AWS::StackName}-API
        Stack:
          Fn::Sub: ${AWS::StackName}
    Metadata:
      SamResourceId: RestAPI

  # user pool for cognito
  UserPool:
    Type: AWS::Cognito::UserPool
    Properties: 
      UserPoolName: !Sub ${AWS::StackName}-UserPool
      AdminCreateUserConfig: 
        AllowAdminCreateUserOnly: false
      AutoVerifiedAttributes: 
        - email
      Schema: 
        - Name: name
          AttributeDataType: String
          Mutable: true
          Required: true
        - Name: email
          AttributeDataType: String
          Mutable: true
          Required: true
      UsernameAttributes: 
        - email
      UserPoolTags:
          Key: Name
          Value: !Sub ${AWS::StackName} User Pool

  # user pool client for cognito
  UserPoolClient:
    Type: AWS::Cognito::UserPoolClient
    Properties: 
      ClientName: 
        !Sub ${AWS::StackName}UserPoolClient
      ExplicitAuthFlows: 
        - ALLOW_USER_PASSWORD_AUTH
        - ALLOW_USER_SRP_AUTH
        - ALLOW_REFRESH_TOKEN_AUTH
      GenerateSecret: false
      PreventUserExistenceErrors: ENABLED
      RefreshTokenValidity: 30
      SupportedIdentityProviders: 
        - COGNITO
      UserPoolId: !Ref UserPool
      AllowedOAuthFlowsUserPoolClient: true
      AllowedOAuthFlows:
        - 'implicit'
      AllowedOAuthScopes:
        - 'email'
        - 'openid'
      CallbackURLs:
        - 'http://localhost:3000/helloarif'

  UserPoolDomain:
    Type: AWS::Cognito::UserPoolDomain
    Properties: 
      Domain: !Ref UserPoolClient
      UserPoolId: !Ref UserPool

  ApiAdministratorsUserPoolGroup:
    Type: AWS::Cognito::UserPoolGroup
    Properties:
      Description: User group for API Administrators
      GroupName: !Ref UserPoolAdminGroupName
      Precedence: 0
      UserPoolId: !Ref UserPool

Outputs:
  PostUrlFunction:
    Description: API Gateway endpoint URL for Prod stage for Post Url function
    Value:
      Fn::Sub: https://${RestAPI}.execute-api.${AWS::Region}.amazonaws.com/Prod/createSecret
  GetByIdFunction:
    Description: API Gateway endpoint URL for Prod stage for Get By User Id Function
    Value:
      Fn::Sub: https://${RestAPI}.execute-api.${AWS::Region}.amazonaws.com/Prod/getbyuserid/{id}
  GetByShortUrlFunction:
    Description: API Gateway endpoint URL for Prod stage for Get By Short Url Function
    Value:
      Fn::Sub: https://${RestAPI}.execute-api.${AWS::Region}.amazonaws.com/Prod/getbyshorturl/{userid}/{shorturl}
  CreateShorturlByUseridFunction:
    Description: API Gateway endpoint URL for Prod stage for add url to existing user
    Value:
      Fn::Sub: https://${RestAPI}.execute-api.${AWS::Region}.amazonaws.com/Prod/addurl/{userid}
  DeleteShortUrlFunction:
    Description: API Gateway endpoint URL for Prod stage for delete url of a user
    Value:
      Fn::Sub: "https://${RestAPI}.execute-api.${AWS::Region}.amazonaws.com/Prod/delete/shorturl/{userid}"
  UpdateShortUrlFunction:
    Description: API Gateway endpoint URL for Prod stage for update url of a user
    Value:
      Fn::Sub: "https://${RestAPI}.execute-api.${AWS::Region}.amazonaws.com/Prod/update/shorturl/{shorturl}"
  UrlRedirectFunction:
    Description: API Gateway endpoint URL for Prod stage for url redirect
    Value:
      Fn::Sub: https://${RestAPI}.execute-api.${AWS::Region}.amazonaws.com/Prod/url/redirect/{shorturl}
  RestAPI:
    Description: RESTAPI
    Value:
      Ref: RestAPI
  UrlTable:
    Description: DynamoDB Table
    Value:
      Ref: UrlTable
  UserPool:
    Description: Cognito User Pool ID
    Value: !Ref UserPool

  UserPoolClient:
    Description: Cognito User Pool Application Client ID
    Value: !Ref UserPoolClient
  CognitoAuthCommnad:
    Description: Cognito Auth Command
    Value: !Sub 'aws cognito-idp initiate-auth --auth-flow USER_PASSWORD_AUTH --client-id ${UserPoolClient} --auth-parameters USERNAME=<user@example.com>,PASSWORD=<password>'
  CognitoLoginURL:
    Description: Cognito Login URL
    Value: !Sub 'https://${UserPoolDomain}.auth.${AWS::Region}.amazoncognito.com/login?response_type=token&client_id=${UserPoolClient}&redirect_uri=http://localhost:3000/helloarif'
  
