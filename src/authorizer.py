import json
import requests
import jwt
import os

def lambda_handler(event, context):
    token = event.get('authorizationToken').split('Bearer ')[-1]
    
    print(token)

    # Add your JWT validation logic here
    if validate_jwt_token(token):
        return generate_policy('user', 'Allow', event['methodArn'])
    else:
        return generate_policy('user', 'Deny', event['methodArn'])

def validate_jwt_token(token):
    # Replace 'your_user_pool_id' with your actual Cognito User Pool ID
    user_pool_id = os.getenv('USER_POOL_ID','None')
    region=os.getenv('REGION','None')
    # Get the public key from Cognito
    public_key_url = f'https://cognito-idp.{region}.amazonaws.com/{user_pool_id}/.well-known/jwks.json'
    public_keys = requests.get(public_key_url).json().get('keys', [])

    # Iterate through public keys and attempt to verify the token
    for public_key in public_keys:
        try:
            # Decode and verify the token
            decoded_token = jwt.decode(token, algorithms=['RS256'], options={"verify_signature": False}, key=public_key)
            # Add additional validation logic if needed
            return True
        except jwt.ExpiredSignatureError:
            print("Token has expired")
            return False
        except jwt.InvalidTokenError:
            continue

    # Token could not be verified with any public key
    return False

def generate_policy(principal_id, effect, resource):
    auth_response = {
        'principalId': principal_id,
        'policyDocument': {
            'Version': '2012-10-17',
            'Statement': [
                {
                    'Action': 'execute-api:Invoke',
                    'Effect': effect,
                    'Resource': resource,
                },
            ],
        },
    }
    return auth_response
