import json
import uuid
import boto3
import os
import secrets
from aws_lambda_powertools import Tracer
from boto3.dynamodb.conditions import Key, Attr
from aws_lambda_powertools.utilities.typing import LambdaContext

URL_TABLE = os.getenv("URL_TABLE", None)
dynamodb = boto3.resource('dynamodb')
ddbTable = dynamodb.Table(URL_TABLE)
# Reuse Boto3 client outside the handler
dynamodb_client = boto3.client('dynamodb')
# Reuse tracer outside the handler
tracer = Tracer()
# Initialize headers outside the handler
headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
}


@tracer.capture_lambda_handler
def lambda_handler(event, context):
    route_key = f"{event['httpMethod']} {event['resource']}"
    response_body=''

    try:
        if route_key == "GET /url/redirect/{shorturl}":
            short_url = "shorturl#" + event['pathParameters']['shorturl']
            status_code = 302

            response = ddbTable.query(
                KeyConditionExpression=Key('pk').eq(short_url)
            )
            if 'Items' in response:
                original_url = response['Items'][0]['original_url']
                click_count = response['Items'][0]['click']

                ddbTable.update_item(
                    Key={
                        'pk': short_url,
                        'sk': short_url
                    },
                    UpdateExpression='SET click = :val1',
                    ExpressionAttributeValues={
                        ':val1': str(int(click_count) + 1)
                    }
                )
                response_body = original_url

    except Exception as err:
        status_code = 400
        response_body = {'Error': str(err)}
        print(str(err))

    return {
        'statusCode': status_code,
        'headers': {
            'Content-Type': 'text/plain',
            'Location': response_body},
        'body': response_body
    }
